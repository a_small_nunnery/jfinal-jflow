package BP.JFINAL;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.FakeStaticHandler;
import com.jfinal.kit.Prop;
import com.jfinal.template.Engine;

import BP.WF.HttpHandler.Base.AttachmentUploadController;
import BP.WF.HttpHandler.Base.CCBPMDesignerController;
import BP.WF.HttpHandler.Base.WF_Comm_Controller;

/**
 * JFinal 配置中心
 * @author asus
 *
 */
public class BpJFinalConfig extends JFinalConfig {
	
	static Prop p;
	
//	/**
//	 * 先加载开发环境配置，然后尝试加载生产环境配置，生产环境配置不存在时不会抛异常
//	 * 在生产环境部署时后动创建 demo-config-pro.txt，添加的配置项可以覆盖掉
//	 * demo-config-dev.txt 中的配置项
//	 */
//	static void loadConfig() {
//		if (p == null) {
//			p = PropKit.use("demo-config-dev.txt").appendIfExists("demo-config-pro.txt");
//		}
//	}
//	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		//loadConfig();
		
		//me.setDevMode(p.getBoolean("devMode", false));
		me.setDevMode(false);
		
		// 支持 Controller、Interceptor 之中使用 @Inject 注入业务层，并且自动实现 AOP
		me.setInjectDependency(true);
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
//		me.add("/", IndexController.class, "/index");	// 第三个参数为该Controller的视图存放路径
//		me.add("/blog", BlogController.class);			// 第三个参数省略时默认与第一个参数值相同，在此即为 "/blog"	
		me.add("/", IndexController.class);
		me.add("/WF/Comm/ProcessRequest", WF_Comm_Controller.class);	 //通用Controller
		me.add("/WF/Admin/CCBPMDesigner/ProcessRequest", CCBPMDesignerController.class);  //设计器所用Controller	
		me.add("/WF/Ath", AttachmentUploadController.class);  
	}
	
	public void configEngine(Engine me) {
//		me.addSharedFunction("/common/_layout.html");
//		me.addSharedFunction("/common/_paginate.html");
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
//		// 配置 druid 数据库连接池插件
//		DruidPlugin druidPlugin = new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
//		me.add(druidPlugin);
//		
//		// 配置ActiveRecord插件
//		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
//		// 所有映射在 MappingKit 中自动化搞定
//		_MappingKit.mapping(arp);
//		me.add(arp);
	}
	
//	public static DruidPlugin createDruidPlugin() {
//		loadConfig();
//		
//		return new DruidPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
//	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		//添加请求处理器，该处理器用于将request实现不依赖Controller的访问
		me.add(new PrefixHandler(".do"));//处理.do结尾的请求
		me.add(new RequestHander());
	}
}
