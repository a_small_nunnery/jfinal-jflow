package BP.WF.HttpHandler.Base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import BP.JFINAL.RequestHander;

/**
 * 工具类的父类。封装框架的特定实现,SpringMvc的实现
 * @author asus
 *
 */
public class CommonUtils {
	/**
	 * JFinal下获取request
	 * 
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		try {
			javax.servlet.http.HttpServletRequest request = RequestHander.getRequest();
		    return request;
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * JFinal下获取response
	 * 
	 * @return
	 */
	public static HttpServletResponse getResponse() {
		try {
			//在请求过滤器中获取请求对象
			return RequestHander.getResponse();
		} catch (Exception e) {
			return null;
		}
	}

}
